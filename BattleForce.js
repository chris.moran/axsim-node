"use strict";

const ForceUnits = require('./ForceUnits');

class BattleForce {

    constructor(attackerForce, defenderForce) {
        this.attackerForce = new ForceUnits(attackerForce, true);
        this.defenderForce = new ForceUnits(defenderForce, false);
    }
}

module.exports = BattleForce
    