


# Since the application not looks for the PORT and DOMAINNAME environment variables, use this command to launch the app:

PORT=8000 DOMAINNAME=localhost node app.js



# Sample Post Data
{
  "simulationResult": {
    "numIterations": 10000,
    "attackerForce": {
      "infantry": 5,
      "artillery": 2,
      "mech_infantry": 1,
      "tank": 0,
      "aagun": 0,
      "fighter": 0,
      "tactical_bomber": 0,
      "bomber": 0,
      "submarine": 0,
      "destroyer": 0,
      "cruiser": 0,
      "carrier": 0,
      "battleship": 0
    },
    "defenderForce": {
      "infantry": 8,
      "artillery": 2,
      "mech_infantry": 1,
      "tank": 0,
      "aagun": 0,
      "fighter": 0,
      "tactical_bomber": 0,
      "bomber": 0,
      "submarine": 0,
      "destroyer": 0,
      "cruiser": 0,
      "carrier": 0,
      "battleship": 0
    }
  }
}

# To build the container image
docker build -t axsim-node:1.0 .

# To run the container image interactively and pass the host and port in as environment variables
docker run --env PORT=8000 --env DOMAINNAME=localhost --interactive --tty --name=axsim-node -p 8000:8000 axsim-node:1.0

# To remove the container after shutting down
docker rm axsim-node