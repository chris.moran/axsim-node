"use strict";

var express = require('express');
var simulationRouter = express.Router();
const BattleForce = require('.././BattleForce');
//const ForceUnits = require('.././ForceUnits');

let attack_index = 0;
let defend_index = 1;
let unitStr = {
    infantry: [1, 2],
    artillery:  [2, 2],
    mech_infantry:  [1, 2],
    tank:  [3, 3],
    aagun:  [0, 1],
    fighter:  [1, 2],
    tactical_bomber:  [3, 4],
    bomber:  [3, 3],
    submarine:  [2, 1],
    destroyer:  [2, 2],
    cruiser:  [3, 3],
    carrier:  [0, 2],
    battleship:  [4, 4],
    // Special unit combination rules
    // Infantry + Artillery → Infantry attacks @ 2
    // Mech. Infantry + Artillery → Mech. Infantry attacks @ 2
    combined_infantry:  [2, 2],
    // Tactical Bomber + Tank → Tactical Bomber attacks @ 4
    // Tactical Bomber + Fighter → Tactical Bomber attacks @ 4
    combined_tactical_bomber:  [3, 4]
    // Plane + Destroyer → Plane can hit Subs
    // use attack_strength and defend_strength to dereference unit strength arrays
};

let rollEachUnit = function (numUnits, unitStr) {
    let hits = 0;
    if (numUnits > 0) {
        for (let i = 0; i < numUnits; i++) {
            let dieRoll = (Math.floor(Math.random() * 6) + 1);
            if (dieRoll <= unitStr) {
                hits += 1;
            }
        }
    }
    return hits;
}

let resolveCombatRolls = function (force, isAttacker) {
    let hits = 0;
    let index = defend_index;
    if (isAttacker) {
        index = attack_index;
    }

    // Todo: resolve offshore bombardments
        
    // Todo: resolve AA guns

    hits += rollEachUnit(force.infantry, unitStr.infantry[index]);
    hits += rollEachUnit(force.combined_infantry, unitStr.combined_infantry[index]);
    hits += rollEachUnit(force.artillery, unitStr.artillery[index]);
    hits += rollEachUnit(force.mech_infantry, unitStr.mech_infantry[index]);
    hits += rollEachUnit(force.tank, unitStr.tank[index]);
    hits += rollEachUnit(force.fighter, unitStr.fighter[index]);
    hits += rollEachUnit(force.tactical_bomber, unitStr.tactical_bomber[index]);
    hits += rollEachUnit(force.combined_tactical_bomber, unitStr.combined_tactical_bomber[index]);
    hits += rollEachUnit(force.bomber, unitStr.bomber[index]);
    hits += rollEachUnit(force.submarine, unitStr.submarine[index]);
    hits += rollEachUnit(force.destroyer, unitStr.destroyer[index]);
    hits += rollEachUnit(force.cruiser, unitStr.cruiser[index]);
    hits += rollEachUnit(force.carrier, unitStr.carrier[index]);
    hits += rollEachUnit(force.battleship, unitStr.battleship[index]);
    return hits;
}

let resolveHits = function(hits, force) {
    //console.log("         resolving " + hits + " hits")
    //console.log("              before applying hist: " + force.getUnitStr());
    for (let i = 0; i < hits; i++) {
        if (force.aagun > 0) {
            force.aagun -= 1;
        } else if (force.infantry > 0) {
            force.infantry -= 1;
        } else if (force.mech_infantry > 0) {
            force.mech_infantry -= 1;
        } else if (force.combined_infantry > 0) {
            force.combined_infantry -= 1;
        } else if (force.artillery > 0) {
            force.artillery -= 1;
        } else if (force.tank > 0) {
            force.tank -= 1;
        } else if (force.battleship > 0 && force.damaged_battleship < force.battleship) {
            // if there are undamaged battleships, add one count to the number of damaged battleships
            force.damaged_battleship += 1;
        } else if (force.fighter > 0) {
            force.fighter -= 1;
        } else if (force.tactical_bomber > 0) {
            force.tactical_bomber -= 1;
        } else if (force.combined_tactical_bomber > 0) {
            force.combined_tactical_bomber -= 1;
        } else if (force.bomber > 0) {
            force.bomber -= 1;
        } else if (force.submarine > 0) {
            force.submarine -= 1;
        } else if (force.destroyer > 0) {
            force.destroyer -= 1;
        } else if (force.cruiser > 0) {
            force.cruiser -= 1;
        } else if (force.carrier > 0) {
            force.carrier -= 1;
        } else if (force.damaged_battleship > 0) {
            // The only way you get here is if the number of damaged battleships is equal to the number of battleships
            // then you strart decrementing battleships
            force.battleship -= 1;
        }
    }
    //console.log("              after applying hist: " + force.getUnitStr());
    return force;
}

simulationRouter.post('/api/api', function (req, res, next) {  
    //console.log('Running Simulation: ' );
    
    //console.log('Artillery attack strength: ' + unitStr.artillery[attack_index]);
    //console.log('Attacker numbers: ' + getUnitStr(req.body.simulationResult.attackerForce));

    let battleForce = new BattleForce(req.body.simulationResult.attackerForce, req.body.simulationResult.defenderForce);
    //console.log('    Attacker numbers: ' + battleForce.attackerForce.getUnitStr());
    //console.log('    Defender numbers: ' + battleForce.defenderForce.getUnitStr());
    
    //let results = req.body.simulationResult;
    let attacker_wins = 0;
    let defender_wins = 0;
    let runNumTimes = req.body.simulationResult.numIterations;
    let longestBattle = 0;
    for (let i = 0; i < runNumTimes; i++) {
        // main simulation loop
        // create a new battleforce object from the attacker and defender objects in the result on each loop
        let battleForce = new BattleForce(req.body.simulationResult.attackerForce, req.body.simulationResult.defenderForce);
        let currentBattlelength = 0;
        while(battleForce.attackerForce.getUnitStr() > 0 && battleForce.defenderForce.getUnitStr() > 0) {
            currentBattlelength += 1;
            let attacker_hits = resolveCombatRolls(battleForce.attackerForce, true);
            let defender_hits = resolveCombatRolls(battleForce.defenderForce, false);
            //console.log("  Attacker hits: " + attacker_hits);
            //console.log("  Defender hits: " + defender_hits);
            //console.log("     Resolving defender hits");
            battleForce.attackerForce = resolveHits(defender_hits, battleForce.attackerForce);
            //console.log("     Resolving attacker hits");
            battleForce.defenderForce = resolveHits(attacker_hits, battleForce.defenderForce);
            //console.log("  Attacker Strength: " + battleForce.attackerForce.getUnitStr());
            //console.log("  Defender Strength: " + battleForce.defenderForce.getUnitStr());
        }
        if (currentBattlelength > longestBattle) {
            longestBattle = currentBattlelength;
        }
        if (battleForce.attackerForce.getUnitStr() > 0) {
            attacker_wins += 1;
        } else {
            defender_wins += 1;
        }
    }
    let results = new Object;
    results.attackerWins = attacker_wins/runNumTimes * 100;
    results.defenderWins = defender_wins/runNumTimes * 100;
    results.longestBattle = longestBattle;
    results.numIterations = runNumTimes;
    results.attackerForce = req.body.simulationResult.attackerForce;
    results.defenderForce = req.body.simulationResult.defenderForce;

    res.json(results);
});


module.exports = simulationRouter;


