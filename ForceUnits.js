"use strict";

class ForceUnits {
    constructor (force, isAttacker) {
        this.aagun = force.aagun;
        this.infantry = force.infantry;
        this.artillery = force.artillery;
        this.mech_infantry = force.mech_infantry;
        this.tank = force.tank;
        this.aagun = force.aagun;
        this.fighter = force.fighter;
        this.tactical_bomber = force.tactical_bomber;
        this.bomber = force.bomber;
        this.submarine = force.submarine;
        this.destroyer = force.destroyer;
        this.cruiser = force.cruiser;
        this.carrier = force.carrier;
        this.battleship = force.battleship;
        this.combined_infantry = 0;
        this.combined_tactical_bomber = 0;
        this.damaged_battleship = 0;

        if (isAttacker) {
            if (this.artillery > 0 && this.artillery > 0) {
                let infantryDifference = this.artillery - this.infantry;
                this.combined_infantry = this.artillery;
                if (infantryDifference >= 0) { 
                    // there are more artillery than infantry or the number is the same
                    // all the infantry become combined infantry
                    this.infantry = 0
                } else {
                    this.infantry = this.infantry - this.artillery;
                }
            }
        
            if ((this.tank > 0 || this.fighter > 0) && this.tactical_bomber > 0) {
                let bomberDifference = this.tank + this.fighter - this.tactical_bomber;
                this.combined_tactical_bomber = this.tank + this.fighter;
                if (bomberDifference >= 0) { 
                    // there are more tanks and fighters than tactical bombers or the number is the same
                    // all the tactical bombers become combined tactical bombers
                    this.tactical_bomber = 0
                } else {
                    this.tactical_bomber = this.tactical_bomber - this.tank - this.fighter;
                }
            }
        }
    }

    getUnitStr = function() {
        let unitcount = this.aagun +
                    this.infantry +
                    this.artillery +
                    this.mech_infantry +
                    this.tank +
                    this.aagun +
                    this.fighter +
                    this.tactical_bomber +
                    this.bomber +
                    this.submarine +
                    this.destroyer +
                    this.cruiser +
                    this.carrier +
                    this.battleship +
                    this.combined_tactical_bomber +
                    this.combined_infantry
        return unitcount
    }

    printForceUnits () {
        console.log('   infantry: ' + this.infantry);
        console.log('   artillery: ' + this.artillery);
        console.log('   mech_infantry: ' + this.mech_infantry);
        console.log('   tank: ' + this.tank);
        console.log('   aagun: ' + this.aagun);
        console.log('   fighter: ' + this.fighter);
        console.log('   tactical_bomber: ' + this.tactical_bomber);
        console.log('   bomber: ' + this.bomber);
        console.log('   submarine: ' + this.submarine);
        console.log('   destroyer: ' + this.destroyer);
        console.log('   cruiser: ' + this.cruiser);
        console.log('   carrier: ' + this.carrier);
        console.log('   battleship: ' + this.battleship);
        console.log('   combined_tactical_bomber: ' + this.combined_tactical_bomber);
        console.log('   combined_infantry: ' + this.combined_infantry);
    }


}

module.exports = ForceUnits